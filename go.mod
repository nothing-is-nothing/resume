module gitee.com/nothing-is-nothing/resume

go 1.22.1

require (
	gitee.com/nothing-is-nothing/logger v1.0.0
	github.com/gookit/color v1.5.4
)

require (
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/sys v0.14.0 // indirect
)
