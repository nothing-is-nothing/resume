package main

import (
	"os"
	"time"

	"gitee.com/nothing-is-nothing/logger"
	"gitee.com/nothing-is-nothing/resume"
)

func main() {
	// 初始化进度
	resumeFilename := "resume.example.cfg"
	progress := resume.NewProgress(resumeFilename)
	startIndex, err := progress.Resume()
	if err != nil {
		logger.Error("progress.Resume() failed: %v\n", err)
		os.Exit(1)
	}
	//监控退出信号，保存进度
	progress.Watch()

	// 模拟业务处理
	progress.TotalCount = 1000
	for i := startIndex; i < progress.TotalCount; i++ {
		progress.AddCheckedCount()
		progress.ShowProgress()
		if progress.CheckedCount < startIndex {
			continue
		}
		// 模拟业务处理
		time.Sleep(time.Millisecond)
		progress.AddMatchedCount()
	}

}
