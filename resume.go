package resume

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	"gitee.com/nothing-is-nothing/logger"
	"github.com/gookit/color"
)

type Progress struct {
	CheckedCount   int64
	TotalCount     int64
	MatchedCount   int64
	ResumeFilename string
}

func NewProgress(resumeFilename string) *Progress {
	return &Progress{
		CheckedCount:   0,
		ResumeFilename: resumeFilename,
	}
}

func (p *Progress) ShowProgress() {
	str := color.Yellow.Sprintf("\r| MatchedCount:%d |  CheckedCount:%d | TotalCount:%d | Progress:%.2f%% | ", p.MatchedCount, p.CheckedCount, p.TotalCount, float64(p.CheckedCount)/float64(p.TotalCount)*100)
	fmt.Fprintf(os.Stderr, "%s", str)
}

func (p *Progress) AddMatchedCount() {
	atomic.AddInt64(&p.MatchedCount, 1)
}

func (p *Progress) AddCheckedCount() {
	atomic.AddInt64(&p.CheckedCount, 1)
}

func (p *Progress) WriteResumeFile() error {
	f, err := os.OpenFile(p.ResumeFilename, os.O_CREATE|os.O_WRONLY, 0o644)
	if err != nil {
		logger.Error("os.OpenFile failed,err:%+v\n", err)
		return err
	}
	defer f.Close()
	_, err = f.WriteString(fmt.Sprintf("%d\n", p.CheckedCount))
	if err != nil {
		logger.Error("f.WriteString failed,err:+%v\n", err)
		return err
	}
	return nil
}

func (p *Progress) Resume() (startIndex int64, err error) {
	//判断当前目录下是否有 crawler.resume.cfg 文件，如果没有，则从0开始，如果有，则从文件中读取
	if _, err := os.Stat(p.ResumeFilename); err == nil {
		//文件存在
		f, err := os.Open(p.ResumeFilename)
		if err != nil {
			logger.Error("os.Open failed,err:%+v\n", err)
			return 0, err
		}
		defer f.Close()
		//读取文件内容
		line, err := bufio.NewReader(f).ReadString('\n')
		if err != nil {
			logger.Error("bufio.NewReader failed,err:%+v\n", err)
			return 0, err
		}
		//转换为int64
		startIndex, err = strconv.ParseInt(strings.TrimSpace(line), 10, 64)
		if err != nil {
			logger.Error("strconv.Atoi failed,err:%+v\n", err)
			return 0, err
		}
	}
	logger.Error("startIndex:%d\n", startIndex)
	return startIndex, nil
}

func (p *Progress) Watch() {
	//监听退出信号，如果有退出信号，则记录一个 crawler.resume.cfg 文件
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt)
	go func() {
		for {
			select {
			case <-signalCh:
				logger.Error("received interrupt signal, saving resume.cfg\n")
				//保存当前的checkedCount
				p.WriteResumeFile()
				os.Exit(0)
			default:
				time.Sleep(time.Second)
			}
		}
	}()
}
